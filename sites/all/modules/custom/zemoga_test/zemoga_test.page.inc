<?php

/**
 * Implements hook_form().
 *  Callback zemoga-user-register
 */
function zemoga_register_form($form, &$form_state) {

  //Init or continue Step Form
  $form_state['step'] = isset($form_state['step']) ? $form_state['step'] : 1;
 
  $form['#prefix'] = '<div id="wrap-form-register">';
  $form['#suffix'] = '</div>';

  $function_step = '_zemoga_register_step_' . $form_state['step'] . '_form';
  $function_step($form, $form_state);
  
  //Create a container for our buttons
  $form['buttons'] = array(
    '#type' => 'container',
  );

  //Back Button
  if($form_state['step'] !== 1) {
    $form['buttons']['back'] = array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#limit_validation_errors' => array(),
      '#submit' => array('zemoga_register_form_back_submit'),
      '#ajax' => array(
        'wrapper' => 'wrap-form-register',
        'callback' => 'zemoga_register_form_ajax_callback',
      ),
    );
  }

  //Next Button
  if($form_state['step'] !== 2) {
    $form['buttons']['forward'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#ajax' => array(
        'wrapper' => 'wrap-form-register',
        'callback' => 'zemoga_register_form_ajax_callback',
      ),
    );
  }else {
    //Submit Button for the Form
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#ajax' => array(
        'wrapper' => 'wrap-form-register',
        'callback' => 'zemoga_register_form_ajax_callback',
      ),
    );
  }
 
  return $form;
}


/*
 * Fields for Step 1
 */
function _zemoga_register_step_1_form(&$form, &$form_state) {
  $df_name      = '';
  $df_last_name = '';
  $df_genre     = 'woman';
  $df_birthdate = array('day' => 1, 'month' => 1, 'year' => 1980);

  if(isset($form_state['values']['name']) && isset($form_state['values']['last_name']) && isset($form_state['values']['genre']) && isset($form_state['values']['birthdate'])) {

    $df_name      = $form_state['values']['name'];
    $df_last_name = $form_state['values']['last_name'];
    $df_genre     = $form_state['values']['genre'];
    $df_birthdate = $form_state['values']['birthdate'];

  }else if(isset($form_state['storage']['name']) && isset($form_state['storage']['last_name']) && isset($form_state['storage']['genre']) && isset($form_state['storage']['birthdate'])) {
    
    $df_name      = $form_state['storage']['name'];
    $df_last_name = $form_state['storage']['last_name'];
    $df_genre     = $form_state['storage']['genre'];
    $df_birthdate = $form_state['storage']['birthdate'];
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#description' => t('First name of the User'),
    '#required' => TRUE,
    '#default_value' => $df_name,
  );

  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#description' => t('Last Name of the User'),
    '#required' => TRUE,
    '#default_value' => $df_last_name,
  );

  $options_genre = array(
    t('Woman') => t('Woman'),
    t('Man') => t('Man'),
  );
  $form['genre'] = array(
    '#title' => t('Genre'),
    '#description' => t('Genre of the User'),
    '#type' => 'select',
    '#options' => $options_genre,
    '#default_value' => $df_genre,
    '#required' => TRUE,
  );

  $form['birthdate'] = array(
    '#type' => 'date',
    '#title' => t('Birthdate'),
    '#description' => t('Birth Date of the User'),
    '#default_value' => $df_birthdate,
  );

  return $form;
}

/*
 * Fields for Step 2
 */
function _zemoga_register_step_2_form(&$form, &$form_state) {

  $df_city    = '';
  $df_phone   = '';
  $df_address = '';
  
  if(isset($form_state['values']['city']) && isset($form_state['values']['phone']) && isset($form_state['values']['address'])) {

    $df_city    = $form_state['values']['city'];
    $df_phone   = $form_state['values']['phone'];
    $df_address = $form_state['values']['address'];

  }else if(isset($form_state['storage']['city']) && isset($form_state['storage']['phone']) && isset($form_state['storage']['address'])) {
    
    $df_city    = $form_state['storage']['city'];
    $df_phone   = $form_state['storage']['phone'];
    $df_address = $form_state['storage']['address'];
  }

  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#description' => t('City of the User'),
    '#required' => TRUE,
    '#default_value' => $df_city,
  );

  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#description' => t('Phone of the User'),
    '#required' => FALSE,
    '#default_value' => $df_phone,
    '#element_validate' => array('element_validate_number'),
  );

  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#description' => t('Address of the User'),
    '#required' => FALSE,
    '#default_value' => $df_address,
  );
    
  return $form;
}
 

/*
 * Callback Back Button
 */
function zemoga_register_form_back_submit($form, &$form_state) {
  $form_state['step']--;
  $form_state['rebuild'] = TRUE;
}
 

/*
 * Submit Button Callback
 */
function zemoga_register_form_submit($form, &$form_state) {
  $step = $form_state['step'];

  switch ($step) {
    case 1:
      $form_state['storage']['name']      = $form_state['values']['name'];
      $form_state['storage']['last_name'] = $form_state['values']['last_name'];
      $form_state['storage']['genre']     = $form_state['values']['genre'];
      $form_state['storage']['birthdate'] = $form_state['values']['birthdate'];
      break;

    case 2:
      $form_state['storage']['city']    = $form_state['values']['city'];
      $form_state['storage']['phone']   = $form_state['values']['phone'];
      $form_state['storage']['address'] = $form_state['values']['address'];
      break;
  }
 
  //Last Step
  if(isset($form_state['values']['forward']) && $form_state['values']['op'] == $form_state['values']['forward']) {
    $form_state['step']++;
  }
  //Submit to Create User
  elseif(isset($form_state['values']['submit']) && $form_state['values']['op'] == $form_state['values']['submit']) {

    $birthdate = $form_state['storage']['birthdate'];
    $birthdate_format = $birthdate['day'] . '/' . $birthdate['month'] . '/' . $birthdate['year'];

    //Create User
    $new_user = array(
      'name' => $form_state['storage']['name'],
      'mail' => $form_state['storage']['name'] . "@test-zemoga.com",
      'status' => 1,
      'field_first_name' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['name']))),
      'field_last_name' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['last_name']))),
      'field_gender' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['genre']))),
      'field_date_of_birth' => array(LANGUAGE_NONE => array(array('value' => $birthdate_format))),
      'field_city' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['city']))),
      'field_phone_number' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['phone']))),
      'field_address' => array(LANGUAGE_NONE => array(array('value' => $form_state['storage']['address']))),
      'access' => REQUEST_TIME,
      'roles' => array(DRUPAL_AUTHENTICATED_RID => 'authenticated user'), // No other roles than Authenticated
    );
    $result_create = user_save(NULL, $new_user);
    if ($result_create) {
      unset($form_state['storage']['birthdate']);
      $items = $form_state['storage'];
      array_push($items, $birthdate_format);

      drupal_set_message(t('You have successfully registered, with the following data:!values', array('!values' => theme('item_list', array('items' => $items)))));
    }else {
      form_set_error(t('An error occurred while registering the user, try later.'));
    }      

    //Return to Step 1
    $form_state['step'] = 1;
    $form_state['storage'] = array();
  }
 
  $form_state['rebuild'] = TRUE;
}
 
//Ajax Callback Return Form
function zemoga_register_form_ajax_callback($form, &$form_state) {
  return $form;
}